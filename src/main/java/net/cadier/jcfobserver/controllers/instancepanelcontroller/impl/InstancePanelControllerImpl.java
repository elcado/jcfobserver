/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcfobserver.controllers.instancepanelcontroller.impl;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import net.cadier.fxworkbench.fxutils.FXUtils;
import net.cadier.fxworkbench.fxwindow.fxmlcontroller.impl.FXMLControllerImpl;
import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.ProvidedService;
import net.cadier.jcf.lang.instance.Instance;
import net.cadier.jcf.lang.instance.events.EventsSink;
import net.cadier.jcf.lang.instance.events.EventsSource;
import net.cadier.jcf.lang.instance.services.ImplementedService;
import net.cadier.jcf.lang.instance.services.UsedService;
import net.cadier.jcfobserver.controllers.instancepanelcontroller.InstancePanelController;
import net.cadier.jcfobserver.controllers.instancepanelcontroller.InstancePanelControllerServices;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

@ComponentImplementation
public class InstancePanelControllerImpl extends FXMLControllerImpl implements InstancePanelController {
    private static Integer titledPaneInternalPadding = 10;

    @FXML
    private TextField instanceIdTextField = null;

    @FXML
    private TitledPane servicesTitledPane = null;

    @FXML
    private Label implementedServicesLabel = null;

    @FXML
    private ListView<ImplementedService> implementedServicesListView = null;

    @FXML
    private Label usedServicesLabel = null;

    @FXML
    private ListView<UsedService> usedServicesListView = null;

    @FXML
    private TitledPane eventsTitledPane = null;

    @FXML
    private Label eventsSourcesLabel = null;

    @FXML
    private ListView<EventsSource> eventsSourcesListView = null;

    @FXML
    private Label eventsSinksLabel = null;

    @FXML
    private ListView<EventsSink> eventsSinksListView = null;

    @ProvidedService
    public final transient InstancePanelControllerServices instancePanelControllerServices = instance -> {
        if (Objects.isNull(instance)) {
            FXUtils.runLater(this::resetPanel);
        } else {
            FXUtils.runLater(() -> this.modelToView(instance));
        }
    };

    private void modelToView(Instance instance) {
        // instance id
        instanceIdTextField.setText(instance.getInstanceId().toString());

        // implemented services
        implementedServicesListView.setItems(FXCollections.observableArrayList(instance.getImplementedServices()));

        // used services
        usedServicesListView.setItems(FXCollections.observableArrayList(instance.getUsedServices()));

        // events sources
        eventsSourcesListView.setItems(FXCollections.observableArrayList(instance.getEventsSources()));

        // events sinks
        eventsSinksListView.setItems(FXCollections.observableArrayList(instance.getEventsSinks()));
    }

    private void resetPanel() {
        instanceIdTextField.setText("");
        implementedServicesListView.getItems().clear();
        usedServicesListView.getItems().clear();
    }

    @Override
    public URL getFXMLUrl() throws MalformedURLException {
        return new URL("file:///D:/Users/Fred/Documents/IdeaProjects/jCF/jCFObserver/src/main/resources/jcfInstancePanel.fxml");
    }

    @Override
    public Object getFXControllerBean() {
        return this;
    }

    @Override
    public void onFXMLLoaded() {
        servicesTitledPane.widthProperty().addListener((observable, oldValue, newValue) -> {
            usedServicesLabel.setLayoutX(newValue.doubleValue() / 2);

            implementedServicesListView.setPrefWidth(newValue.doubleValue() / 2 - implementedServicesLabel.getPrefWidth() - titledPaneInternalPadding*2);
            usedServicesListView.setPrefWidth(newValue.doubleValue() / 2 - usedServicesLabel.getPrefWidth() - titledPaneInternalPadding*2);
        });

        eventsTitledPane.widthProperty().addListener((observable, oldValue, newValue) -> {
            eventsSinksLabel.setLayoutX(newValue.doubleValue() / 2);

            eventsSourcesListView.setPrefWidth(newValue.doubleValue() / 2 - eventsSourcesLabel.getPrefWidth() - titledPaneInternalPadding*2);
            eventsSinksListView.setPrefWidth(newValue.doubleValue() / 2 - eventsSinksLabel.getPrefWidth() - titledPaneInternalPadding*2);
        });

        implementedServicesListView.setCellFactory(listView -> new ListCell<>() {
            @Override
            protected void updateItem(ImplementedService implementedService, boolean empty) {
                super.updateItem(implementedService, empty);
                setText(implementedService == null ? "" : implementedService.getClazz().getCanonicalName());
            }
        });

        usedServicesListView.setCellFactory(listView -> new ListCell<>() {
            @Override
            protected void updateItem(UsedService usedService, boolean empty) {
                super.updateItem(usedService, empty);
                setText(usedService == null ? "" : usedService.getClazz().getCanonicalName());
            }
        });

        eventsSourcesListView.setCellFactory(listView -> new ListCell<>() {
            @Override
            protected void updateItem(EventsSource eventsSource, boolean empty) {
                super.updateItem(eventsSource, empty);
                setText(eventsSource == null ? "" : eventsSource.getEventClazz().getCanonicalName());
            }
        });

        eventsSinksListView.setCellFactory(listView -> new ListCell<>() {
            @Override
            protected void updateItem(EventsSink eventsSink, boolean empty) {
                super.updateItem(eventsSink, empty);
                setText(eventsSink == null ? "" : eventsSink.getEventClazz().getCanonicalName());
            }
        });
    }
}