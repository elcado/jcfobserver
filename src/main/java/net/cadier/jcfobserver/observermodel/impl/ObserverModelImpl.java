/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcfobserver.observermodel.impl;

import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.EventSource;
import net.cadier.jcf.annotations.ProvidedService;
import net.cadier.jcf.annotations.UsedService;
import net.cadier.jcf.core.container.ContainerDebugServices;
import net.cadier.jcf.lang.component.Startable;
import net.cadier.jcf.lang.instance.Instance;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.events.Source;
import net.cadier.jcfobserver.observermodel.ObserverModel;
import net.cadier.jcfobserver.observermodel.ObserverModelEvent;
import net.cadier.jcfobserver.observermodel.ObserverModelServices;
import net.cadier.utils.tree.Tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ComponentImplementation
public class ObserverModelImpl implements ObserverModel, ObserverModelServices, Startable {

    private InstanceId localContainerId = null;
    private final List<InstanceId> remoteContainersIds = new ArrayList<>();

    private final Map<InstanceId, Tree<InstanceId, Instance>> containersIds2Tree = new HashMap<>();

    private InstanceId currentSelectedContainerId;

    private Instance currentSelectedInstance;

    @ProvidedService
    public ObserverModelServices observerModelServices = this;

    @UsedService
    private final ContainerDebugServices containerDebugServices = null;

    @EventSource
    public Source<ObserverModelEvent> observerModelEventSource = null;

    @Override
    public boolean onStarted() {
        // update model
        // TODO use containerDebugServices.getLocalContainerId() when available
        Tree<InstanceId, Instance> localInstancesTree = containerDebugServices.getLocalInstancesTree();
        this.localContainerId = new InstanceId();
        containersIds2Tree.put(this.localContainerId, localInstancesTree);

        // notify
        observerModelEventSource.postEvent(new ObserverModelEvent(ObserverModelEvent.ObserverModelEventType.CONTAINERS_UPDATED));

        return true;
    }

    @Override
    public boolean onPaused() {
        return true;
    }

    @Override
    public boolean onResumed() {
        return true;
    }

    @Override
    public boolean onStopped() {
        return true;
    }

    @Override
    public boolean onFailed() {
        return true;
    }

    @Override
    public void setCurrentSelectedContainerId(InstanceId containerId) {
        this.currentSelectedContainerId = containerId;

        // notify
        observerModelEventSource.postEvent(new ObserverModelEvent(ObserverModelEvent.ObserverModelEventType.CONTAINER_SELECTION_CHANGED));
    }

    @Override
    public InstanceId getCurrentSelectedContainerId() {
        return this.currentSelectedContainerId;
    }

    @Override
    public InstanceId getLocalContainerId() {
        return this.localContainerId;
    }

    @Override
    public List<InstanceId> getRemoteContainersIds() {
        return this.remoteContainersIds;
    }

    @Override
    public Tree<InstanceId, Instance> getInstancesTree(final InstanceId containerId) {
        return containersIds2Tree.get(containerId);
    }

    @Override
    public Instance getCurrentSelectedInstance() {
        return currentSelectedInstance;
    }

    @Override
    public void setCurrentSelectedInstance(Instance instance) {
        this.currentSelectedInstance = instance;

        // notify
        observerModelEventSource.postEvent(new ObserverModelEvent(ObserverModelEvent.ObserverModelEventType.INSTANCE_SELECTION_CHANGED));
    }
}
