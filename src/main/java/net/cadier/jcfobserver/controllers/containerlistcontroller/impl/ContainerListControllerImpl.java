/*
 * Copyright (c). Frédéric Cadier (2014-2020)
 *
 * This software is a computer program whose purpose is to allow component programming in pure java.
 *
 * This software is governed by the CeCILL-C license under French law and abiding by the rules of distribution of
 * free software. You can use, modify and/or redistribute the software under the terms of the CeCILL-C license as
 * circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the
 * license, users are provided only with a limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 *
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific status of free software, that may mean
 *  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL-C license and that
 * you accept its terms.
 */

package net.cadier.jcfobserver.controllers.containerlistcontroller.impl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.ListView;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.Pane;
import net.cadier.fxworkbench.fxutils.FXUtils;
import net.cadier.fxworkbench.fxwindow.fxmlcontroller.FXMLControllerServices;
import net.cadier.fxworkbench.fxwindow.fxmlcontroller.impl.FXMLControllerImpl;
import net.cadier.jcf.annotations.ComponentImplementation;
import net.cadier.jcf.annotations.EventSink;
import net.cadier.jcf.annotations.UsedService;
import net.cadier.jcf.lang.instance.Instance;
import net.cadier.jcf.lang.instance.InstanceId;
import net.cadier.jcf.lang.instance.events.Sink;
import net.cadier.jcfobserver.controllers.containerlistcontroller.ContainerListController;
import net.cadier.jcfobserver.controllers.instancepanelcontroller.InstancePanelControllerServices;
import net.cadier.jcfobserver.controllers.instancepanelcontroller.impl.InstancePanelControllerImpl;
import net.cadier.jcfobserver.observermodel.ObserverModelEvent;
import net.cadier.jcfobserver.observermodel.ObserverModelServices;
import net.cadier.utils.tree.Tree;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@ComponentImplementation(composition = {InstancePanelControllerImpl.class})
public class ContainerListControllerImpl extends FXMLControllerImpl implements ContainerListController {
    @FXML
    private ListView<InstanceId> containerList = null;

    @FXML
    private TreeView<Instance> containerView = null;

    @FXML
    private Pane instanceViewPane = null;

    @UsedService
    protected final transient ObserverModelServices observerModelServices = null;

    @UsedService
    protected final transient FXMLControllerServices instancePanelFXMLControllerServices = null;

    @UsedService
    protected final transient InstancePanelControllerServices instancePanelControllerServices = null;

    @EventSink
    private final transient Sink<ObserverModelEvent> fxModelEventsSink = this::processEvent;

    private InstanceId localContainerId;
    private List<InstanceId> remoteContainersIds;

    @Override
    public URL getFXMLUrl() throws MalformedURLException {
        return new URL("file:///D:/Users/Fred/Documents/IdeaProjects/jCF/jCFObserver/src/main/resources/jcfObserverMainPanel.fxml");
    }

    @Override
    public Parent loadFXML() {
        Parent parent = super.loadFXML();

        // instance panel
        Parent instancePanel = instancePanelFXMLControllerServices.loadFXML();
        instanceViewPane.getChildren().add(instancePanel);

        return parent;
    }

    @Override
    public Object getFXControllerBean() {
        return this;
    }

    @Override
    public void onFXMLLoaded() {
        containerView.setShowRoot(false);
        containerView.setCellFactory(instanceTreeView -> new TreeCell<>() {
            @Override
            protected void updateItem(Instance instance, boolean empty) {
                super.updateItem(instance, empty);
                setText(instance == null ? "" : instance.getComponent().getClazz().getCanonicalName());
            }
        });

        this.setListeners();
    }

    private void setListeners() {
        containerList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            this.observerModelServices.setCurrentSelectedContainerId(newValue);
        });

        containerView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (Objects.isNull(newValue)) {
                this.observerModelServices.setCurrentSelectedInstance(null);
            } else {
                this.observerModelServices.setCurrentSelectedInstance(newValue.getValue());
            }
        });
    }

    private void processEvent(ObserverModelEvent observerModelEvent) {
        switch (observerModelEvent.getEventType()) {
            case CONTAINERS_UPDATED:
                this.localContainerId = observerModelServices.getLocalContainerId();
                this.remoteContainersIds = observerModelServices.getRemoteContainersIds();

                ObservableList<InstanceId> containersListItems = FXCollections.observableArrayList();
                containersListItems.add(this.localContainerId);
                containersListItems.addAll(this.remoteContainersIds);
                containerList.setItems(containersListItems);

                break;

            case CONTAINER_SELECTION_CHANGED:
                InstanceId currentSelectedContainer = observerModelServices.getCurrentSelectedContainerId();
                if (Objects.isNull(currentSelectedContainer)) {
                    // reset tree view and instance panel view
                    this.setContainerViewRoot(null);
                } else if (currentSelectedContainer.equals(this.localContainerId)) {
                    // set local container tree as current
                    this.setCurrentlyViewedContainer(this.observerModelServices.getInstancesTree(currentSelectedContainer));
                } else {
                    // TODO set a remote container tree as current
                }

                break;

            case INSTANCE_SELECTION_CHANGED:
                this.instancePanelControllerServices.setCurrentInstance(this.observerModelServices.getCurrentSelectedInstance());

                break;
        }
    }

    private void setCurrentlyViewedContainer(final Tree<InstanceId, Instance> instancesTree) {
        // dummy root
        TreeItem<Instance> root = new TreeItem<>(null);

        // set our instances' tree roots
        instancesTree.getRoots().stream()
                .map(rootId -> createInstanceTreeItem(instancesTree, rootId))
                .forEach(root.getChildren()::add);

        this.setContainerViewRoot(root);
    }

    /**
     * Create a TreeItem for the supplied Instance. This TreeItem will have children if this Instance does.
     * @param instancesTree tree of instances we're building a TreeView for.
     * @param instanceId id of the Instance we're building a TreeItem for.
     * @return a new TreeItem.
     */
    private TreeItem<Instance> createInstanceTreeItem(final Tree<InstanceId, Instance> instancesTree,
                                                      final InstanceId instanceId) {
        TreeItem<Instance> treeItem = new TreeItem<>(instancesTree.getRef(instanceId)) {
            // flag to ensure that we only add children once
            AtomicBoolean isFirstTimeChildren = new AtomicBoolean(true);

            @Override
            public ObservableList<TreeItem<Instance>> getChildren() {
                if (isFirstTimeChildren.compareAndSet(true, false)) {
                    ObservableList<TreeItem<Instance>> children = this.getInstancesChildrenIds().stream()
                            .map(childId -> createInstanceTreeItem(instancesTree,childId))
                            .collect(Collectors.toCollection(FXCollections::observableArrayList));
                    super.getChildren().setAll(children);
                }
                return super.getChildren();
            }

            @Override
            public boolean isLeaf() {
                return this.getInstancesChildrenIds().isEmpty();
            }

            private List<InstanceId> getInstancesChildrenIds() {
                return instancesTree.getChildren(this.getValue().getInstanceId());
            }
        };

        // tree item is expanded by default
        treeItem.setExpanded(true);

        return treeItem;
    }

    private void setContainerViewRoot(final TreeItem<Instance> rootTreeItem) {
        FXUtils.runLater(() -> containerView.setRoot(rootTreeItem));
    }
}